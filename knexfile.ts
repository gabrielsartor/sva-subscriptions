import { join } from 'path'
import dbConfig from './src/Infrastructure/db/config'

export default {
  client: dbConfig.client,
  debug: process.env.NODE_ENV === 'development',
  connection: {
    host: dbConfig.host,
    port: dbConfig.port,
    user: dbConfig.user,
    password: dbConfig.password,
    database: dbConfig.database
  },
  migrations: {
    tableName: 'migrations',
    directory: join(__dirname, './src/Infrastructure/db/migrations')
  },
  seeds: {
    directory: join(__dirname, './src/Infrastructure/db/seeds')
  }
}
