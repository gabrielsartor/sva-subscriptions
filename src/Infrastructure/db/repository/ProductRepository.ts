import { Knex } from 'knex'
import { Product, ProductMap } from '../../../Domain/product'
import { IProductRepository } from '../../../Domain/product/ProductRepository'

export class ProductRepository implements IProductRepository {
  private knex: Knex
  public static tableName = 'product'

  constructor (knex: Knex) {
    this.knex = knex
  }

  public async create (product: Product): Promise<void> {
    await this.knex(ProductRepository.tableName)
      .insert(ProductMap.toPersistence(product))
  }

  public async update (product: Product): Promise<void> {
    await this.knex(ProductRepository.tableName)
      .where({ code: product.code })
      .update(ProductMap.toPersistence(product))
  }

  public async removeByContract (contract: string): Promise<void> {
    await this.knex(ProductRepository.tableName)
      .where({ contract })
      .delete()
  }

  public async findByContract (code: string): Promise<Product> {
    const product = await this.knex(ProductRepository.tableName).where({ code }).first()

    if (!product) {
      return null
    }

    return ProductMap.toDomain(product)
  }
}
