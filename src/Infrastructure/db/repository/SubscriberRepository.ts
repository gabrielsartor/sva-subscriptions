import { Knex } from 'knex'
import { Subscriber, SubscriberMap } from '../../../Domain/subscriber'
import { ISubscriberRepository } from '../../../Domain/subscriber/SubscriberRepository'

export class SubscriberRepository implements ISubscriberRepository {
  private knex: Knex
  public static tableName = 'subscriber'

  constructor (knex: Knex) {
    this.knex = knex
  }

  public async create (subscriber: Subscriber): Promise<void> {
    await this.knex(SubscriberRepository.tableName)
      .insert(SubscriberMap.toPersistence(subscriber))
  }

  public async update (subscriber: Subscriber): Promise<void> {
    await this.knex(SubscriberRepository.tableName)
      .where({ code: subscriber.code })
      .update(SubscriberMap.toPersistence(subscriber))
  }

  public async remove (code: string): Promise<void> {}

  public async findByCode (code: string): Promise<Subscriber> {
    const subscriber = await this.knex(SubscriberRepository.tableName).where({ code }).first()

    if (!subscriber) {
      return null
    }

    return SubscriberMap.toDomain(subscriber)
  }
}
