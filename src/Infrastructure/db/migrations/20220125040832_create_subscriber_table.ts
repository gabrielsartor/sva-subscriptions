import { Knex } from 'knex'

export async function up (knex: Knex): Promise<void> {
  return knex.schema.createTable('subscriber', table => {
    table.string('_id', 36).unique().notNullable()
    table.enum('status', ['active', 'inactive', 'blocked']).defaultTo('active')
    table.string('code', 50).notNullable()
    table.string('reference', 100).notNullable()
    table.string('email', 100).notNullable()
    table.string('name', 100).notNullable()
    table.string('cellphone', 100).notNullable()
    table.dateTime('created_at').defaultTo(knex.raw('NOW()'))
    table.dateTime('updated_at').defaultTo(knex.raw('NOW()'))
  })
}

export async function down (knex: Knex): Promise<void> {
  return knex.schema.dropTable('subscriber')
}
