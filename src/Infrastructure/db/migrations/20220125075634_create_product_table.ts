import { Knex } from 'knex'

export async function up (knex: Knex): Promise<void> {
  return knex.schema.createTable('product', table => {
    table.string('_id', 36).unique().notNullable()
    table.string('code', 50).notNullable()
    table.string('name', 200).notNullable()
    table.string('type', 100).notNullable()
    table.string('contract', 50).notNullable()
    table.dateTime('created_at').defaultTo(knex.raw('NOW()'))
    table.dateTime('updated_at').defaultTo(knex.raw('NOW()'))
  })
}

export async function down (knex: Knex): Promise<void> {
  return knex.schema.dropTable('product')
}
