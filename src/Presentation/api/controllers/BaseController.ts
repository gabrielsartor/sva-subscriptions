import { BaseError } from '../../../Domain/shared/errors/BaseError'
import { HttpRequest, HttpResponse } from '../protocols/Http'
import { ErrorResponse } from '../response/Error'

export abstract class BaseController {
  protected req: HttpRequest
  protected res: HttpResponse

  protected abstract handle(): Promise<void>

  public async execute (req: HttpRequest, res: HttpResponse): Promise<void> {
    this.req = req
    this.res = res

    try {
      await this.handle()
    } catch (error) {
      console.error(error)
      this.fail(error)
    }
  }

  // --------------------------------------------------------------------------

  public created () {
    return this.res.sendStatus(201)
  }

  public deleted () {
    return this.res.sendStatus(204)
  }

  public ok (data?: any) {
    if (data) {
      this.res.type('application/json')

      return this.res.status(200).json(data)
    } else {
      this.res.sendStatus(200)
    }
  }

  public fail (error: BaseError | BaseError[]) {
    const isArray = Array.isArray(error) && error.length > 1
    const errors = Array.isArray(error) ? error : [error]

    const errorsResponse = errors.reduce((errorResponse: ErrorResponse[], error) => {
      errorResponse.push({
        error: error.name,
        type: error.isOperational ? 'operational' : 'unexpected',
        code: error.httpCode,
        message: error.message
      })

      return errorResponse
    }, [])

    this.res
      .status(errorsResponse[0].code || 500)
      .json(isArray ? { errors: errorsResponse } : errorsResponse[0])
  }
}
