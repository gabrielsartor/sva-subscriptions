import { UpsertProductsUseCase } from '../../../Application/product/UpsertProductsUseCase'
import { UpsertSubscriberUseCase } from '../../../Application/subscriber/UpsertSubscriberUseCase'
import { Product } from '../../../Domain/product'
import { BadInput } from '../../../Domain/shared/errors'
import { Subscriber } from '../../../Domain/subscriber'
import { knex } from '../../../Infrastructure/db/queryBuilder'
import { ProductRepository } from '../../../Infrastructure/db/repository/ProductRepository'
import { SubscriberRepository } from '../../../Infrastructure/db/repository/SubscriberRepository'
import { LiveModeAdapter } from '../../../Main/services/adapters/LiveModeAdapter'
import { BaseController } from './BaseController'

export class RegisterSubscriberController extends BaseController {
  protected async handle (): Promise<void> {
    const { status, code, reference, name, email, cellphone, products } = this.req.body

    const errors = Subscriber.validationErrors({ status, code, reference, name, email, cellphone })

    if (errors.length) return this.fail(errors)

    // ------------------------------------------------------------------------

    const subscriber = Subscriber.create({ status, code, reference, name, email, cellphone })

    await new UpsertSubscriberUseCase(new SubscriberRepository(knex)).execute(subscriber)

    // ------------------------------------------------------------------------

    if (Array.isArray(products) === false) {
      throw new BadInput('Products must be an array with products')
    }

    const productsEntities: Product[] = products.reduce((acc: Product[], curr: { code: any; name: any; type: any; contract: any }) => {
      acc.push(Product.create({
        code: curr.code,
        name: curr.name,
        type: curr.type,
        contract: curr.contract
      }))

      return acc
    }, [])

    await new UpsertProductsUseCase(new ProductRepository(knex)).execute(productsEntities)

    // ------------------------------------------------------------------------

    // Foi mal por isso, preciso ir dormir

    const activePaulistao = products.filter((product: { code: string }) => product.code === 'internet-750').length > 0

    if (activePaulistao) {
      const liveMode = new LiveModeAdapter(subscriber)

      liveMode.subscribe()
    }

    this.created()
  }
}
