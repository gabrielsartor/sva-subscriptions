export interface HttpResponse {
  status: (code: number) => HttpResponse
  json?: (body: any) => HttpResponse
  sendStatus?: (code: number) => HttpResponse
  type?: (type: string) => HttpResponse
}

export interface HttpRequest {
  body?: any
  params?: any
  headers?: {
    'user-agent'?: string
    authorization?: string
  }
  query?: any
}
