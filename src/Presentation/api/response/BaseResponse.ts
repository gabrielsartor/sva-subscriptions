export type BaseResponse = {
  [key: string]: any | Array<{[key: string]: any}>
}
