import { BaseResponse } from './BaseResponse'

export interface ErrorResponse extends BaseResponse {
  error: string
  type: 'operational' | 'unexpected'
  code: number
  message: string
}

export type ErrorsResponse = { errors: ErrorResponse[] }
