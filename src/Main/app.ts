import app from './server/config/app'
import { adapter } from './services'

const port = process.env.SERVER_PORT || 3030

app.listen(port, () => console.log(`🚀 Server running on port ${port}`))

// console.log(adapter('LiveModeAdapter'))
