import { Router } from 'express'
import { RegisterSubscriberController } from '../../../Presentation/api/controllers'

const registerSubscriberController = new RegisterSubscriberController()

export default (router: Router): void => {
  router.post('/subscribe', (req, res) => registerSubscriberController.execute(req, res))
}
