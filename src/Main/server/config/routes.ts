import fs from 'fs'
import { join } from 'path'
import { Express, Router } from 'express'

export default (app: Express): void => {
  const router = Router()

  app.use(router)

  fs.readdirSync(join(__dirname, '..', 'routes')).map(async file => {
    (await import(`./../routes/${file}`)).default(router)
  })
}
