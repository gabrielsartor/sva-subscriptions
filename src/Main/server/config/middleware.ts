import cors from 'cors'
import express, { Express } from 'express'

export default (app: Express): void => {
  app.use(express.json({ strict: false }))
  app.use(cors())
}
