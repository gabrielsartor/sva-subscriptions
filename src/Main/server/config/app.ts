import express from 'express'
import setupRoutes from './routes'
import setupSentry from './sentry'
import setupMiddleware from './middleware'

const app = express()

setupSentry(app)
setupMiddleware(app)
setupRoutes(app)

export default app
