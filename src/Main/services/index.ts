import { LiveModeAdapter } from './adapters/LiveModeAdapter'

export const ListPackages = {
  LiveModeAdapter
}

export type Packages = typeof ListPackages

export const adapter = <T extends keyof Packages> (adapter: T, ...options: any[]) : T extends keyof Packages ? InstanceType<Packages[T]> : Error => {
  if (!Object.keys(ListPackages).includes(adapter)) {
    return new Error('Adapter package not found') as any
  }

  // @ts-ignore
  return new ListPackages[adapter](...options) as any
}
