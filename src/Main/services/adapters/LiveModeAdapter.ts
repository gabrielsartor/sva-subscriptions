import axios, { AxiosInstance } from 'axios'
import { Subscriber } from '../../../Domain/subscriber'
import { BaseService } from '../BaseService'

export class LiveModeAdapter extends BaseService {
  private subscriber: Subscriber

  protected axios: AxiosInstance

  constructor (subscriber: Subscriber) {
    super()

    this.subscriber = subscriber
    this.axios = axios.create({
      baseURL: process.env.LIVE_API,
      timeout: 5000,
      headers: { Authorization: 'Bearer 8d4f1cc923d7878a810bf646e2dc06ce' }
    })
  }

  public async subscribe (): Promise<void> {
    try {
      await this.axios.get(`/notification/activate?code=${process.env.LIVE_CODE}&key=${this.subscriber.reference}`)
      console.log('[Paulistão play] subscribe', this.subscriber)
    } catch (error) {
      console.log('error', error)
    }
  }

  public async unsubscribe (): Promise<void> {
    await this.axios.get(`/notification/deactivate?code=${process.env.LIVE_CODE}&key=${this.subscriber.reference}`)
  }
}
