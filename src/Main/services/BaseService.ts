export abstract class BaseService {
  abstract subscribe(): Promise<void>
  abstract unsubscribe(): Promise<void>
}
