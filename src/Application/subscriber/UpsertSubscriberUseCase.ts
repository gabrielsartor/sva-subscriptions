import { Subscriber } from '../../Domain/subscriber'
import { ISubscriberRepository } from '../../Domain/subscriber/SubscriberRepository'

export class UpsertSubscriberUseCase {
  private repository: ISubscriberRepository

  constructor (repository: ISubscriberRepository) {
    this.repository = repository
  }

  async execute (subscriber: Subscriber) {
    const findSubscriber = await this.repository.findByCode(subscriber.code)

    if (findSubscriber) {
      await this.repository.update(subscriber)

      return
    }

    await this.repository.create(subscriber)
  }
}
