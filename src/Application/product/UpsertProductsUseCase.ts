import { Product } from '../../Domain/product'
import { IProductRepository } from '../../Domain/product/ProductRepository'

export class UpsertProductsUseCase {
  private repository: IProductRepository

  constructor (repository: IProductRepository) {
    this.repository = repository
  }

  async execute (products: Product[]) {
    await this.repository.removeByContract(products[0].contract)

    console.log(products)

    for (const product of products) {
      await this.repository.create(product)
    }
  }
}
