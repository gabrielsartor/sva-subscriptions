import { SubscriberProps } from '.'
import { BadInput } from '../shared/errors'
import { BaseError } from '../shared/errors/BaseError'
import { CellPhone, Name, UniqueEntityID } from '../shared/objectValues'

/**
 * Subscriber entity
 */
export class Subscriber {
  private readonly _id: string

  public status: 'active' | 'inactive' | 'blocked' = 'active'
  public code: string
  public reference: string
  public name: Name
  public email: string
  public cellphone: CellPhone

  /**
   * Create a new instance
   * @param props
   * @param id
   */
  private constructor (props: {
    status: 'active' | 'inactive' | 'blocked',
    code: string,
    reference: string,
    name: Name,
    email: string,
    cellphone: CellPhone,
  }, id: string) {
    this._id = id

    this.status = props.status
    this.code = props.code
    this.reference = props.reference
    this.name = props.name
    this.email = props.email
    this.cellphone = props.cellphone
  }

  /**
   * Returns validation errors
   * @param props
   */
  public static validationErrors (props: SubscriberProps): BaseError[] {
    const errors: BaseError[] = []

    if (!props.code) errors.push(new BadInput('code is required'))
    if (!props.reference) errors.push(new BadInput('reference is required'))
    if (!props.email) errors.push(new BadInput('email is required'))

    try { Name.create(props.name) } catch (error) { errors.push(error) }
    try { CellPhone.create(props.cellphone) } catch (error) { errors.push(error) }

    return errors
  }

  /**
   * Creates a new subscriber entity
   * @param props
   */
  public static create (props: SubscriberProps, id?: string): Subscriber {
    if (Subscriber.validationErrors(props).length) throw new BadInput('Invalid Params')

    const uuid = id || UniqueEntityID.create().value

    return new Subscriber({
      status: props.status || 'active',
      code: props.code,
      reference: props.reference,
      name: Name.create(props.name),
      email: props.email,
      cellphone: CellPhone.create(props.cellphone)
    }, uuid)
  }

  // --------------------------------------------------------------------------

  /**
   * Returns the entity id
   */
  public get id () {
    return this._id
  }
}
