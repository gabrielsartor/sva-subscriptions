import { Subscriber } from '.'

export interface ISubscriberRepository {
  create(subscriber: Subscriber): Promise<void>
  update(subscriber: Subscriber): Promise<void>
  remove(code: string): Promise<void>

  findByCode(code: string): Promise<Subscriber>
}
