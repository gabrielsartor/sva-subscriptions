import { Subscriber } from '.'

describe('Entity/Subscriber', () => {
  describe('validation', () => {
    it('should return errors with invalid data', () => {
      const errors = Subscriber.validationErrors({
        code: '',
        reference: '',
        name: '',
        email: '',
        cellphone: ''
      })

      expect(errors.length).toBeGreaterThan(0)
      expect(errors[0].name).toBe('Bad Input')
    })
  })

  describe('create', () => {
    it('should fail if receive invalid data', () => {
      const create = () => Subscriber.create({
        code: '',
        reference: '',
        name: '',
        email: '',
        cellphone: ''
      })

      expect(create).toThrowError()

      try {
        create()
      } catch (error) {
        expect(error).toBeInstanceOf(Error)
        expect(error.name).toBe('Bad Input')
      }
    })

    it('should create with valid data', () => {
      const subscriber = Subscriber.create({
        status: 'blocked',
        code: '123',
        reference: '00011122233',
        name: 'Jhon Doe',
        email: 'jhon@doe.com',
        cellphone: '4499990000'
      }, '54321')

      expect(subscriber).toBeInstanceOf(Subscriber)
      expect(subscriber.status).toBe('blocked')
      expect(subscriber.code).toBe('123')
      expect(subscriber.reference).toBe('00011122233')
      expect(subscriber.name.fullName).toBe('Jhon Doe')
      expect(subscriber.email).toBe('jhon@doe.com')
      expect(subscriber.cellphone.number).toBe('999990000')
      expect(subscriber.id).toBe('54321')
    })
  })
})
