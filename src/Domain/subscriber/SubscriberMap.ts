import { Subscriber } from '.'

/**
 * Props for create a new Subscriber
 */
export interface SubscriberProps {
  status?: 'active' | 'inactive' | 'blocked'
  code: string
  reference: string
  name: string
  email: string
  cellphone: string
}

/**
 * Subscriber DTO
 */
export interface SubscriberDTO {
  status?: 'active' | 'inactive' | 'blocked'
  code: string
  reference: string
  email: string
  name: {
    firstName: string
    lastName: string
    fullName: string
  }
  cellphone: {
    ddi: string
    ddd: string
    number: string
  }
}

export interface SubscriberPersistence {

}

/**
 * Subscriber Mapper
 */
export class SubscriberMap {
  public static toDomain (raw: any) {
    return Subscriber.create({
      status: raw.status,
      code: raw.code,
      reference: raw.reference,
      email: raw.email,
      name: raw.name,
      cellphone: raw.cellphone
    }, raw?._id || undefined)
  }

  public static toPersistence (props: Subscriber): any {
    return {
      _id: props.id,
      status: props.status,
      code: props.code,
      reference: props.reference,
      email: props.email,
      name: props.name.fullName,
      cellphone: `${props.cellphone.ddd} ${props.cellphone.number}`,
      updated_at: new Date()
    }
  }

  public static toDTO (props: Subscriber) {
    // return {
    //   code: props.code,
    //   name: props.name,
    //   type: props.type,
    //   contract: props.contract
    // }
  }
}
