import { BaseError } from './BaseError'

export class BadInput extends BaseError {
  public constructor (
    description: string,
    name = 'Bad Input',
    httpCode = 400
  ) {
    super(name, description, httpCode)
  }
}
