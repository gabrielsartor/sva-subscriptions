import { BaseError } from './BaseError'

export class UnexpectedError extends BaseError {
  public constructor (
    description: string,
    name = 'Unexpected Error',
    httpCode = 500,
    isOperational = false
  ) {
    super(name, description, httpCode, isOperational)
  }
}
