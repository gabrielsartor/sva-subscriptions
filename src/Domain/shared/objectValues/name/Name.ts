import { BadInput } from '../../errors'

interface NameProps {
  name: string
  first: string
  last: string
  middle: string[]
}

export class Name {
  private readonly name: string

  private first: string
  private last: string
  private middle: string[]

  private constructor (props: NameProps) {
    this.name = props.name

    this.first = props.first
    this.last = props.last
    this.middle = props.middle
  }

  private static split (name: string) {
    const portions = name.split(' ')

    return {
      first: portions[0],
      middle: portions.slice(1, -1),
      last: portions[portions.length - 1]
    }
  }

  public static isValid (name: string): void {
    if (!name || name.length < 3 || name.length > 100) {
      throw new BadInput('Name must be between 2 and 100 characters.')
    }
  }

  public static create (name: string) {
    this.isValid(name)

    return new Name({ name, ...Name.split(name) })
  }

  // --------------------------------------------------------------------------

  /**
   * Returns the full name
   */
  public get fullName () {
    return this.name
  }

  /**
   * Returns the truncated name, the credit card style
   */
  public get truncatedName () {
    const middleNameInitials = this.middle.reduce((acc, curr) => acc.concat(curr[0].toUpperCase()), [])

    return `${this.first} ${middleNameInitials.length ? `${middleNameInitials.join(' ')} ` : ''}${this.last}`
  }

  /**
   * Returns the first name
   */
  public get firstName () {
    return this.first
  }

  /**
   * Returns the last name
   */
  public get lastName () {
    return this.last
  }
}
