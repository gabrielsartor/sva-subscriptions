import { Name } from './index'

describe('ObjectValue/Name', () => {
  describe('create', () => {
    it('should fail if receive invalid data', () => {
      const create = () => Name.create('Jh')

      expect(create).toThrowError()

      try {
        create()
      } catch (error) {
        expect(error).toBeInstanceOf(Error)
        expect(error.name).toBe('Bad Input')
      }
    })

    it('should create with valid data', () => {
      const name = Name.create('Jhon')

      expect(name).toBeInstanceOf(Name)
    })
  })

  describe('getters', () => {
    const name = Name.create('Jhon Henry Ranz Doe')

    it('should returns fullName', () => {
      expect(name.fullName).toBe('Jhon Henry Ranz Doe')
    })

    it('should return truncated name', () => {
      expect(name.truncatedName).toBe('Jhon H R Doe')
    })

    it('should returns the first name', () => {
      expect(name.firstName).toBe('Jhon')
    })

    it('should returns the last name', () => {
      expect(name.lastName).toBe('Doe')
    })
  })
})
