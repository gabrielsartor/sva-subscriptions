import { nanoid } from 'nanoid'
import { UnexpectedError } from '../../errors'

export class UniqueEntityID {
  private readonly id: string

  private constructor (id: string) {
    this.id = id
  }

  static create () {
    try {
      return new UniqueEntityID(nanoid())
    } catch (error) {
      console.error(error)

      throw new UnexpectedError('Unexpected Error on generate uuid')
    }
  }

  get value (): string {
    return this.id
  }
}
