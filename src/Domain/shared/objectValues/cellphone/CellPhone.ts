import { BadInput } from '../../errors'

export class CellPhone {
  private readonly cellphone: string
  private static readonly regex: any = /^0?(?:(?:\+|00)?([1-9][0-9])\s?)?(?:\(?([1-9][0-9])\)?\s?)(?:((?:9\d|[2-9])\d{3}\d{4}))$/

  private constructor (cellphone: string) {
    this.cellphone = cellphone
  }

  public static isValid (cellphone: string) {
    if (CellPhone.regex.test(cellphone) === false) {
      throw new BadInput('Invalid phone, use format <ddi?><ddd><number> eg: 44999990000')
    }
  }

  public static create (cellphone: string) {
    this.isValid(cellphone)

    return new CellPhone(cellphone)
  }

  /**
   * Returns the ddi
   */
  public get ddi () {
    return this.cellphone.match(CellPhone.regex)[1] || '55'
  }

  /**
   * Returns the ddd
   */
  public get ddd () {
    return this.cellphone.match(CellPhone.regex)[2]
  }

  /**
   * Returns the number with 9 digit
   */
  public get number () {
    const number = this.cellphone.match(CellPhone.regex)[3]
    return number.length === 8 ? `9${number}` : number
  }
}
