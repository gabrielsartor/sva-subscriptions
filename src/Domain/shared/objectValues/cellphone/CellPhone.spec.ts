import { CellPhone } from './index'

describe('ObjectValue/Cellphone', () => {
  describe('create', () => {
    it('should fail if receive a invalid data', () => {
      const create = () => CellPhone.create('449912345')

      expect(create).toThrowError()

      try {
        create()
      } catch (error) {
        expect(error).toBeInstanceOf(Error)
        expect(error.name).toBe('Bad Input')
      }
    })

    it('should create with valid data', () => {
      expect(CellPhone.create('5444998256897')).toBeInstanceOf(CellPhone)
      expect(CellPhone.create('4499990000')).toBeInstanceOf(CellPhone)
      expect(CellPhone.create('44 99990000')).toBeInstanceOf(CellPhone)
      expect(CellPhone.create('(44) 99990000')).toBeInstanceOf(CellPhone)
    })
  })

  describe('getters', () => {
    it('should returns ddi', () => {
      expect(CellPhone.create('4499990000').ddi).toBe('55')
      expect(CellPhone.create('044999990000').ddi).toBe('55')
      expect(CellPhone.create('544499990000').ddi).toBe('54')
    })

    it('should returns ddd', () => {
      expect(CellPhone.create('4599990000').ddd).toBe('45')
      expect(CellPhone.create('044999990000').ddd).toBe('44')
      expect(CellPhone.create('544499990000').ddd).toBe('44')
    })

    it('should returns number', () => {
      expect(CellPhone.create('44999990000').number).toBe('999990000')
      expect(CellPhone.create('4499990000').number).toBe('999990000')
      expect(CellPhone.create('044999990000').number).toBe('999990000')
    })
  })
})
