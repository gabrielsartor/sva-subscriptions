import { ProductProps } from '.'
import { BadInput } from '../shared/errors'
import { UniqueEntityID } from '../shared/objectValues'

/**
 * Product entity
 */
export class Product {
  private readonly _id: string

  public code: string
  public name: string
  public type: string
  public contract: string

  /**
   * Creates a new instance
   * @param props
   * @param id
   */
  private constructor (props: ProductProps, id: string) {
    this._id = id

    this.code = props.code
    this.name = props.name
    this.type = props.type
    this.contract = props.contract
  }

  /**
   * Returns validation errors
   * @param props
   */
  public static validationErrors (props: ProductProps): Error[] {
    const errors: Error[] = []

    if (!props.code) errors.push(new BadInput('code is required'))
    if (!props.name) errors.push(new BadInput('name is required'))
    if (!props.type) errors.push(new BadInput('type is required'))
    if (!props.contract) errors.push(new BadInput('contract is required'))

    return errors
  }

  /**
   * Creates a new product entity
   * @param props
   */
  public static create (props: ProductProps, id?: string): Product {
    if (Product.validationErrors(props).length) throw new BadInput('Invalid params')

    const uuid = id || UniqueEntityID.create().value

    return new Product({
      code: props.code,
      name: props.name,
      type: props.type,
      contract: props.contract
    }, uuid)
  }

  // --------------------------------------------------------------------------

  /**
   * Returns the entity id
   */
  public get id () {
    return this._id
  }
}
