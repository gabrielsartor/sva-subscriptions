import { Product } from '.'

export interface IProductRepository {
  create(product: Product): Promise<void>
  update(product: Product): Promise<void>
  removeByContract(contract: string): Promise<void>

  findByContract(contract: string): Promise<Product>
}
