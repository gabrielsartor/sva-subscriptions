import { Product } from '.'

/**
 * Product props
 */
export interface ProductProps {
  code: string
  name: string
  type: string
  contract: string
}

/**
 * Product DTO
 */
export interface ProductDTO {
  code: string
  name: string
  type: string
  contract: string
}

/**
 * Product Mapper
 */
export class ProductMap {
  public static toDomain (raw: any): Product {
    return Product.create({
      code: raw.code,
      name: raw.name,
      type: raw.type,
      contract: raw.contract
    }, raw?.id || undefined)
  }

  public static toPersistence (props: Product): any {
    return {
      _id: props.id,
      code: props.code,
      name: props.name,
      type: props.type,
      contract: props.contract,
      updated_at: new Date()
    }
  }

  public static toDTO (props: Product): ProductDTO {
    return {
      code: props.code,
      name: props.name,
      type: props.type,
      contract: props.contract
    }
  }
}
