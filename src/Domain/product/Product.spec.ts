import { Product } from '.'

describe('Entity/Product', () => {
  describe('validation', () => {
    it('should return errors with invalid data', () => {
      const errors = Product.validationErrors({ code: '', name: '', type: '', contract: '' })

      expect(errors.length).toBeGreaterThan(0)
      expect(errors[0].name).toBe('Bad Input')
    })
  })

  describe('create', () => {
    it('should fail if receive invalid data', () => {
      const create = () => Product.create({
        code: '',
        name: '',
        type: '',
        contract: ''
      })

      expect(create).toThrowError()

      try {
        create()
      } catch (error) {
        expect(error).toBeInstanceOf(Error)
        expect(error.name).toBe('Bad Input')
      }
    })

    it('should create with valid data', () => {
      const product = Product.create({
        code: '12345',
        name: 'Internet 250mb',
        type: 'internet',
        contract: '12345'
      })

      expect(product).toBeInstanceOf(Product)
      expect(product.code).toBe('12345')
      expect(product.name).toBe('Internet 250mb')
      expect(product.type).toBe('internet')
      expect(product.contract).toBe('12345')
    })

    it('should create with id', () => {
      const product = Product.create({
        code: '12345',
        name: 'Internet 250mb',
        type: 'internet',
        contract: '12345'
      }, '123')

      expect(product).toBeInstanceOf(Product)
      expect(product.code).toBe('12345')
      expect(product.name).toBe('Internet 250mb')
      expect(product.type).toBe('internet')
      expect(product.contract).toBe('12345')
      expect(product.id).toBe('123')
    })
  })
})
